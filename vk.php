<?php
error_reporting(0);
$offer = null;
foreach ($product_export['shop']['offers'] as $value) {
    if (isset($value['product_id'])) {
        if ($is_id == $value['product_id']) {
            $offer = $value;
            break;
        }
    }
}

$export_url = downloadImage(stripslashes($offer["picture"]));

//Telegram_debug_Jenya::send_debug($task['task'] . "\nexport_url: " . $export_url);
$token = getTokenExecutor($token);
usleep(250000);

$upload_url = stripslashes($vk->photos()->getMarketUploadServer($token, [
    'group_id' => $group_id,
    'main_photo' => 1,
    'v' => '5.126'
])["upload_url"]);

//Telegram_debug_Jenya::send_debug($task['task'] . "\nupload_url: " . $upload_url);
//$token = getTokenExecutor($token);
usleep(250000);

$ch = curl_init();
$file = curl_file_create($export_url, 'image/png', 'temp.png');
$parameters = [
    'file' => $file
];

curl_setopt($ch, CURLOPT_URL, $upload_url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$photo = json_decode(curl_exec($ch), true);
curl_close($ch);

//Telegram_debug_Jenya::send_debug($task['task'] . "\nPHOTO['PHOTO']: " . $photo['photo']);
//Telegram_debug_Jenya::send_debug($task['task'] . "\nPHOTO['PHOTO'] stripslashes : " . stripslashes($photo['photo']));
//$token = getTokenExecutor($token);
usleep(250000);

$photo_id = $vk->photos()->saveMarketPhoto($token, [
    'group_id' => $group_id,
    'photo' => stripslashes($photo['photo']),
    'server' => $photo['server'],
    'hash' => $photo['hash'],
    'crop_data' => $photo['crop_data'],
    'crop_hash' => $photo['crop_hash'],
    //'v' => '5.120'
])[0]['id'];

//Telegram_debug_Jenya::send_debug($task['task'] . "\nphoto_id: " . $photo_id);
//$token = getTokenExecutor($token);
usleep(250000);

$description = getDescription($offer);

if($offer['price'] == 0 || $offer['full_price'] == 0) {
    $offer['price'] = 1;
    $offer['full_price'] = 1;
}
$product_id = $vk->market()->add($token, [
    'owner_id' => $owner_id,
    'item_id' => $vk_id,
    'name' => $offer['name'],
    'description' => $description,
    'category_id' => 1103,
    'price' => $offer['price'],
    'old_price' => $offer['full_price'],
    'sku' => $offer['product_id'],
    'main_photo_id' => $photo_id,
    
    'v' => '5.131'
]);

//Telegram_debug_Jenya::send_debug($task['task'] . "\nresult add: " . $product_id);
//$token = getTokenExecutor($token);
usleep(250000);

$albums = $vk->market()->getAlbums($token, [
    'owner_id' => $owner_id,
    'count' => '100',
    'v' => '5.139'
]);

//Telegram_debug_Jenya::send_debug($task['task'] . "\nalbums: " . $albums);
//$token = getTokenExecutor($token);
usleep(250000);

$category_q = $sql->query("SELECT c.name FROM product_categories_list as c 
    LEFT OUTER JOIN product_list as l
    ON l.product_id = '" . $offer['product_id'] . "'
    WHERE c.id = l.category_id") or die($sql->error());
$category_array = mysqli_fetch_assoc($category_q);

$category_name = $category_array['name'];

$album_id = null;

foreach ($albums['items'] as $value) {
    if (html_entity_decode($value['title']) == html_entity_decode($category_name)) {
        $album_id = $value['id'];
        break;
    }
}

$result = $vk->market()->addToAlbum($token, [
    'owner_id' => $owner_id,
    'item_id' => $product_id,
    'album_ids' => $album_id
]);

Telegram_debug_Jenya::send_debug($task['task'] . "\nresult: " . json_encode($result, JSON_UNESCAPED_UNICODE));
