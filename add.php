
<?php
// Код, касающийся нашей инфосистемы убран

// Ссылка на изображение которое нужно загрузить
$export_url = downloadImage(stripslashes($offer["picture"]));

$token = getTokenExecutor($token);
usleep(100000);


// Ссылка куда нужно загрузить изображение
$upload_url = $vk->photos()->getMarketUploadServer($token, [
    'group_id' => $group_id,
    'main_photo' => 1,
    'v' => '5.126'
])["upload_url"];

$token = getTokenExecutor($token);
usleep(100000);


// Загрузка изображения
$ch = curl_init();
$file = curl_file_create($export_url, 'image/png', 'temp.png');
$parameters = [
    'file' => $file,
    'access_token' => $token,
    'v' => '5.126'
];

curl_setopt($ch, CURLOPT_URL, $upload_url);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $parameters);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
// Ответ от ВК после загрузки изображения
$photo = json_decode(curl_exec($ch), true);
curl_close($ch);

$token = getTokenExecutor($token);
usleep(100000);


// Сохранение изображения
$photo_id = $vk->photos()->saveMarketPhoto($token, [
    'group_id' => $group_id,
    'photo' => stripslashes($photo['photo']),
    'server' => $photo['server'],
    'hash' => $photo['hash'],
    'crop_data' => $photo['crop_data'],
    'crop_hash' => $photo['crop_hash'],
    'v' => '5.126'
])[0]['id'];

$token = getTokenExecutor($token);
usleep(100000);

$description = getDescription($offer);


// Создание товара
$product_id = $vk->market()->add($token, [
    'owner_id' => $owner_id,
    'item_id' => $vk_id,
    'name' => $offer['name'],
    'description' => $description,
    'category_id' => 1103,
    'price' => $offer['price'],
    'old_price' => $offer['full_price'],
    'sku' => $offer['product_id'],
    'main_photo_id' => $photo_id,
    'v' => '5.131'
]);

$token = getTokenExecutor($token);
usleep(100000);


$albums = $vk->market()->getAlbums($token, [
    'owner_id' => $owner_id,
    'count' => '100',
    'v' => '5.139'
]);

$token = getTokenExecutor($token);
usleep(100000);

$result = $vk->market()->addToAlbum($token, [
    'owner_id' => $owner_id,
    'item_id' => $product_id,
    'album_ids' => $album_id
]);
